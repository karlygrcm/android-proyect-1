package com.example.asynctask.task;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

public class Helper extends AsyncTask<String, String, String> {

	String json = null;
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		HttpClient cliente =  new DefaultHttpClient();
		HttpGet Get = new HttpGet(params[0]);
		
		try {
			HttpResponse respuesta = cliente.execute(Get);
			HttpEntity ent = respuesta.getEntity();
			json = EntityUtils.toString(ent);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Log.i("excepcion lanzada", "buh");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	
}
